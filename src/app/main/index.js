// imports
import React, { Fragment, useState, useEffect } from "react";

// material
import { makeStyles } from "@material-ui/core/styles";

// mapbox
import MapGL, { Source, Layer } from "react-map-gl";
import style from "./data/style";
import Pins from "./components/Pins";

// data
import data from "./data/data";
import pins from "./data/pins/pins.json";

// component
import Panel from "./components/Panel";
import Report from "./components/Report";
import HealthRep from "./components/HealthReport";

const MAPBOX_TOKEN = "pk.eyJ1IjoiazIzIiwiYSI6ImNrZHlpeGFsbTExNDgzMHFpMWh0aW4wb2QifQ._Os1vIXrEuuLqt1dQcPf6w";

const useStyles = makeStyles(() => ({
	root: {
		width: "100vw",
		height: "100vh",
		position: "absolute",
		left: "0%",
		top: "0%",
		zIndex: 0
	}
}));

const Dashboard = () => {
	const classes = useStyles();
	const [viewport, setViewPort] = useState({
		latitude: 19.39907077374756,
		longitude: 4.700262390191065,
		zoom: 1.5,
		bearing: 0,
		pitch: 0
	});
	const [year, setYear] = useState(0);
	const [gas, setGas] = useState(0);
	const [tempR, setTempR] = useState(0);
	const [open, setOpen] = useState(false);
	const [loc, setLoc] = useState({});
	const [openRep, setOpenRep] = useState(false);

	const _onViewportChange = (viewport) => {
		setViewPort({ ...viewport });
	};

	const changeViewPort = (e) => {
		setViewPort({
			latitude: e.lngLat[1],
			longitude: e.lngLat[0],
			zoom: 6,
			bearing: 0,
			pitch: 0
		});
	};
	const _onClickMarker = (data) => {
		setLoc({ ...data });
		if (data.Country === "India") {
			setOpenRep(true);
		} else {
			setOpen(true);
		}
	};

	useEffect(() => {
		alert("Please Wait for 30sec till Heatmap gets Loaded!!!");
	}, [gas, year]);

	return (
		<Fragment>
			<div className={classes.root}>
				<MapGL
					{...viewport}
					width="100%"
					height="100%"
					mapStyle="mapbox://styles/mapbox/dark-v9"
					onViewportChange={(e) => _onViewportChange(e)}
					mapboxApiAccessToken={MAPBOX_TOKEN}
					onClick={(e) => changeViewPort(e)}>
					{data[year] && (
						<Source type="geojson" data={data[year]}>
							<Layer {...style[gas]} />
						</Source>
					)}
					<Pins data={pins} onClick={_onClickMarker} />
				</MapGL>
			</div>
			<Panel year={year} gas={gas} tempR={tempR} setTempR={setTempR} setYear={setYear} setGas={setGas} openRep={openRep} setOpenRep={setOpenRep} />
			<Report setOpen={setOpen} open={open} pinLoc={loc.Country} />
			<HealthRep setOpen={setOpenRep} open={openRep} pinLoc={loc.Country} />
		</Fragment>
	);
};

export default Dashboard;

// imports
import React from "react";
import { Bar } from "react-chartjs-2";

export default function CODough() {
	return (
		<Bar
			data={{
				labels: [
					"Ahamdabad",
					"Aizawl",
					"Amravati",
					"Begaluru",
					"Bhopal",
					"Bajrajnagar",
					"Chandigarh",
					"Chennai",
					"Coimbatore",
					"Delhi",
					"Ernakulam",
					"Gurugram",
					"Guwahati",
					"Hydrabad",
					"Jaipur",
					"Joraphokhar",
					"Kochi",
					"Kolkata",
					"Lucknow",
					"Mumbai",
					"Patna",
					"Shillong",
					"Talchar",
					"Thrivananthapuram",
					"Visakhapatnam"
				],
				datasets: [
					{
						label: "AQI ",
						backgroundColor: "#35495e",
						borderColor: "rgba(0,0,0,0.5)",
						borderWidth: 1,
						data: [380, 60, 70, 110, 95, 130, 135, 70, 65, 62, 61, 247, 100, 158, 251, 98, 112, 160, 125, 175, 220, 150, 218, 75, 218, 80, 105]
					}
				]
			}}
			options={{
				title: {
					display: true,
					text: "AQI Before Lockdown",
					fontSize: 25
				}
			}}
			width={100}
			height={100}
		/>
	);
}

// imports
import React from "react";
import { Bar } from "react-chartjs-2";

export default function CODough() {
	return (
		<Bar
			data={{
				labels: [
					"Ahamdabad",
					"Aizawl",
					"Amravati",
					"Begaluru",
					"Bhopal",
					"Bajrajnagar",
					"Chandigarh",
					"Chennai",
					"Coimbatore",
					"Delhi",
					"Ernakulam",
					"Gurugram",
					"Guwahati",
					"Hydrabad",
					"Jaipur",
					"Joraphokhar",
					"Kochi",
					"Kolkata",
					"Lucknow",
					"Mumbai",
					"Patna",
					"Shillong",
					"Talchar",
					"Thrivananthapuram",
					"Visakhapatnam"
				],
				datasets: [
					{
						label: "AQI ",
						backgroundColor: "#64958f",
						borderColor: "rgba(0,0,0,0.5)",
						borderWidth: 1,
						data: [124, 43, 50, 62, 68, 108, 138, 41, 61, 83, 106, 70, 104, 141, 62, 82, 120, 83, 78, 102, 77, 129, 70, 132, 59, 60]
					}
				]
			}}
			options={{
				title: {
					display: true,
					text: "AQI After Lockdown",
					fontSize: 25
				}
			}}
			width={100}
			height={100}
		/>
	);
}

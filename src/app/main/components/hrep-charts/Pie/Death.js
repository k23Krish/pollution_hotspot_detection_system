// imports
import React from "react";
import { Doughnut } from "react-chartjs-2";

export default function CH4Dough({ pinLoc, data }) {
	return (
		<Doughnut
			data={{
				labels: [
					"Lung Cancer",
					"Acute Lower Respiratory Infection",
					"Death from Stroke",
					"Ischemic Heart Disease",
					"Chronic Obstructive Pulmonary Disease"
				],
				datasets: [
					{
						label: "Death",
						fill: true,
						lineTension: 0.5,
						backgroundColor: ["#463333", "gray", "#834c69", "silver", "#835858", "#cbaf87"],
						borderColor: "rgba(0,0,0,0.5)",
						borderWidth: 1,
						data: [29, 17, 24, 25, 43]
					}
				]
			}}
			options={{
				title: {
					display: true,
					text: "Disease and Death caused by Air Pollution in %",
					fontSize: 25
				},
				legend: {
					display: true,
					position: "right"
				}
			}}
			width={100}
			height={30}
		/>
	);
}

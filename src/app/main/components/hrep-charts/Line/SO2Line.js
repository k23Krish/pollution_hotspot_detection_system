// imports
import React from "react";
import { Line } from "react-chartjs-2";

export default function SO2Line() {
	return (
		<Line
			data={{
				labels: ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"],
				datasets: [
					{
						label: "SO2",
						backgroundColor: "#065c6f",
						borderColor: "rgba(0,0,0,0.5)",
						borderWidth: 1,
						data: [22.8, 22.6, 23, 21.7, 20.8, 18.9, 16.9, 17.1, 17.3, 21.7, 24.7, 20.9],
						fill: false
					}
				]
			}}
			options={{
				title: {
					display: true,
					text: "SO2 Monthly Trend",
					fontSize: 25
				}
			}}
			width={100}
			height={100}
		/>
	);
}

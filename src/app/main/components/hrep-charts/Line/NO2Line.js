// imports
import React from "react";
import { Line } from "react-chartjs-2";

export default function NO2Line() {
	return (
		<Line
			data={{
				labels: ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"],
				datasets: [
					{
						label: "NO2",
						backgroundColor: "#ffbb91",
						borderColor: "rgba(0,0,0,0.5)",
						borderWidth: 1,
						data: [38.3, 35.2, 30, 27.7, 25.5, 23.5, 21.5, 20, 23, 32.5, 38, 35.5],
						fill: false
					}
				]
			}}
			options={{
				title: {
					display: true,
					text: "NO2 Monthly Trend",
					fontSize: 25
				}
			}}
			width={100}
			height={100}
		/>
	);
}

// imports
import React from "react";
import { Line } from "react-chartjs-2";

export default function PM2Line() {
	return (
		<Line
			data={{
				labels: ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"],
				datasets: [
					{
						label: "PM2.5",
						backgroundColor: "#faf3dd",
						borderColor: "rgba(0,0,0,0.5)",
						borderWidth: 1,
						data: [118, 90, 76, 62, 63, 54, 42, 38, 40, 70, 105, 106],
						fill: false
					}
				]
			}}
			options={{
				title: {
					display: true,
					text: "PM2.5 Monthly Trend",
					fontSize: 25
				}
			}}
			width={100}
			height={100}
		/>
	);
}

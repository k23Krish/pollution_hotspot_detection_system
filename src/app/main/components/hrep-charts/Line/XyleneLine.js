// imports
import React from "react";
import { Line } from "react-chartjs-2";

export default function XyleneLine() {
	return (
		<Line
			data={{
				labels: ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"],
				datasets: [
					{
						label: "Xylene",
						backgroundColor: "#347474",
						borderColor: "rgba(0,0,0,0.5)",
						borderWidth: 1,
						data: [32.5, 29, 28.5, 27, 35, 35.1, 30, 32.7, 37, 47, 44, 42],
						fill: false
					}
				]
			}}
			options={{
				title: {
					display: true,
					text: "Xylene Trend",
					fontSize: 25
				}
			}}
			width={100}
			height={100}
		/>
	);
}

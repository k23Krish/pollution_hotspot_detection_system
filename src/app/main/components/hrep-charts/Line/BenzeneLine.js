// imports
import React from "react";
import { Line } from "react-chartjs-2";

export default function BenzeneLine() {
	return (
		<Line
			data={{
				labels: ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"],
				datasets: [
					{
						label: "Benzene",
						backgroundColor: "#ee8572",
						borderColor: "rgba(0,0,0,0.5)",
						borderWidth: 1,
						data: [3.3, 3.1, 1.8, 1.6, 2.05, 2, 1.5, 2.3, 1.8, 2.7, 3.3, 5],
						fill: false
					}
				]
			}}
			options={{
				title: {
					display: true,
					text: "Benzene Trend",
					fontSize: 25
				}
			}}
			width={100}
			height={100}
		/>
	);
}

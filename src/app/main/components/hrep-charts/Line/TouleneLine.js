// imports
import React from "react";
import { Line } from "react-chartjs-2";

export default function TouleneLine() {
	return (
		<Line
			data={{
				labels: ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"],
				datasets: [
					{
						label: "Toulene",
						backgroundColor: "#35495e",
						borderColor: "rgba(0,0,0,0.5)",
						borderWidth: 1,
						data: [8.9, 7.2, 5.8, 5, 6.8, 7, 6, 5.7, 5.8, 8.2, 9.9, 10.9],
						fill: false
					}
				]
			}}
			options={{
				title: {
					display: true,
					text: "Toulene Trend",
					fontSize: 25
				}
			}}
			width={100}
			height={100}
		/>
	);
}

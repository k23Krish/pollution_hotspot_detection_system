// imports
import React from "react";
import { Line } from "react-chartjs-2";

export default function COLine() {
	return (
		<Line
			data={{
				labels: ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"],
				datasets: [
					{
						label: "CO",
						backgroundColor: "#64958f",
						borderColor: "rgba(0,0,0,0.5)",
						borderWidth: 1,
						data: [3.75, 3.85, 3.5, 3.49, 3.28, 3.65, 4.06, 4.8, 4.7, 4.89, 2.97, 2.57],
						fill: false
					}
				]
			}}
			options={{
				title: {
					display: true,
					text: "CO Monthly Trend",
					fontSize: 25
				}
			}}
			width={100}
			height={100}
		/>
	);
}

// imports
import React, { Fragment, useState } from "react";

// material
import { makeStyles } from "@material-ui/core/styles";
import Typography from "@material-ui/core/Typography";
import Button from "@material-ui/core/Button";
import MenuIcon from "@material-ui/icons/Menu";
import MenuOpenIcon from "@material-ui/icons/MenuOpen";
import Paper from "@material-ui/core/Paper";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import ListItemText from "@material-ui/core/ListItemText";
import MenuItem from "@material-ui/core/MenuItem";
import Menu from "@material-ui/core/Menu";
import ArrowDropDownIcon from "@material-ui/icons/ArrowDropDown";

const useStyles = makeStyles(() => ({
	drawer: {
		marginTop: "10px",
		width: "25%",
		position: "absolute",
		background: "silver",
		border: "1px solid gray",
		padding: "5px"
	},
	dropd: {
		display: "inline-table"
	},
	dropd1: {
		border: "1px solid black",
		borderRadius: "5px",
		background: "white",
		minWidth: "250px",
		width: "300px",
		height: "45px"
	}
}));

const Panel = ({ setOpenRep, setGas, setYear, setTempR, openRep, gas, year, tempR }) => {
	const classes = useStyles();
	const [open, setOpen] = useState(false);
	const [anchorEl, setAnchorEl] = useState(null);
	const [anchorEl1, setAnchorEl1] = useState(null);
	const [anchorEl2, setAnchorEl2] = useState(null);
	const options = [2018, 2019, 2020];
	const options1 = ["CO", "NO2", "SO2", "CH4"];
	const options2 = ["Yearly", "Monthly", "Seasonal"];

	return (
		<Fragment>
			<Button
				variant="contained"
				onClick={(e) => {
					setOpen(!open);
				}}
				size="small">
				{open ? <MenuOpenIcon /> : <MenuIcon />}
			</Button>

			{open ? (
				<Paper variant="outlined" elevation={10} className={classes.drawer}>
					<Typography style={{ margin: "10px" }} variant="h6">
						Year:{" "}
						<List className={classes.dropd} component="nav" aria-label="Device settings">
							<ListItem
								className={classes.dropd1}
								button
								aria-haspopup="true"
								aria-controls="lock-menu"
								onClick={(e) => setAnchorEl(e.currentTarget)}>
								<ListItemText primary={+options[year]} />
								<ArrowDropDownIcon />
							</ListItem>
						</List>
						<Menu id="lock-menu" anchorEl={anchorEl} keepMounted open={Boolean(anchorEl)} onClose={(e) => setAnchorEl(null)}>
							{options.map((option, index) => (
								<MenuItem
									style={{ width: "250px" }}
									key={option}
									selected={index === year}
									onClick={(event) => {
										setYear(index);
										setAnchorEl(null);
									}}>
									{option}
								</MenuItem>
							))}
						</Menu>
					</Typography>
					<Typography style={{ margin: "10px" }} variant="h6">
						Gas :{" "}
						<List className={classes.dropd} component="nav" aria-label="Device settings">
							<ListItem
								className={classes.dropd1}
								button
								aria-haspopup="true"
								aria-controls="lock-menu"
								onClick={(e) => setAnchorEl1(e.currentTarget)}>
								<ListItemText primary={options1[gas]} />
								<ArrowDropDownIcon />
							</ListItem>
						</List>
						<Menu id="lock-menu" anchorEl={anchorEl1} keepMounted open={Boolean(anchorEl1)} onClose={(e) => setAnchorEl1(null)}>
							{options1.map((option, index) => (
								<MenuItem
									style={{ width: "250px" }}
									key={option}
									selected={index === gas}
									onClick={(event) => {
										setGas(index);
										setAnchorEl1(null);
									}}>
									{option}
								</MenuItem>
							))}
						</Menu>
					</Typography>
					<Typography style={{ margin: "10px" }} variant="h6">
						Temporal Resolurtion :{" "}
						<List className={classes.dropd} component="nav" aria-label="Device settings">
							<ListItem
								className={classes.dropd1}
								button
								aria-haspopup="true"
								aria-controls="lock-menu"
								onClick={(e) => setAnchorEl2(e.currentTarget)}>
								<ListItemText primary={options2[tempR]} />
								<ArrowDropDownIcon />
							</ListItem>
						</List>
						<Menu id="lock-menu" anchorEl={anchorEl2} keepMounted open={Boolean(anchorEl2)} onClose={(e) => setAnchorEl2(null)}>
							{options2.map((option, index) => (
								<MenuItem
									style={{ width: "250px" }}
									key={option}
									selected={index === tempR}
									onClick={(event) => {
										setTempR(index);
										setAnchorEl2(null);
									}}>
									{option}
								</MenuItem>
							))}
						</Menu>
					</Typography>
					<Button
						size="medium"
						style={{ margin: "10px", background: "#4c4c4c", color: "white" }}
						variant="contained"
						onClick={() => {
							setOpenRep(!openRep);
						}}>
						Health Report
					</Button>
				</Paper>
			) : null}
		</Fragment>
	);
};

export default Panel;

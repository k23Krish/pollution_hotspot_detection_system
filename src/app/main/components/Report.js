// imports
import React from "react";

// material
import { makeStyles } from "@material-ui/core/styles";
import Dialog from "@material-ui/core/Dialog";
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import IconButton from "@material-ui/core/IconButton";
import Typography from "@material-ui/core/Typography";
import CloseIcon from "@material-ui/icons/Close";
import Slide from "@material-ui/core/Slide";
import Paper from "@material-ui/core/Paper";
import Grid from "@material-ui/core/Grid";

// data
import coRep from "../data/report/coRep.json";
import no2Rep from "../data/report/no2Rep.json";
import so2Rep from "../data/report/so2Rep.json";
import ch4Rep from "../data/report/ch4Rep.json";
import pm2Rep from "../data/report/pm2Rep.json";
import pm10Rep from "../data/report/pm10Rep.json";

// Charts
import CODough from "./charts/Doughnut/CODough";
import NO2Dough from "./charts/Doughnut/NO2Dough";
import SO2Dough from "./charts/Doughnut/SO2Dough";
import CH4Dough from "./charts/Doughnut/CH4Dough";
import PM2Dough from "./charts/Doughnut/PM2Dough";
import PM10Dough from "./charts/Doughnut/PM10Dough";
import Bar from "./charts/Bar/chart";

const useStyles = makeStyles((theme) => ({
	appBar: {
		position: "relative",
		background: "	#303030"
	},
	title: {
		marginLeft: theme.spacing(2),
		flex: 1
	},
	title2: {
		textAlign: "center",
		margin: theme.spacing(2),
		flex: 1
	},
	paper: {
		padding: theme.spacing(2),
		textAlign: "center",
		background: "#f3f3f3",
		width: "100%",
		height: "100%",
		border: "1px solid gray"
	}
}));

const Transition = React.forwardRef(function Transition(props, ref) {
	return <Slide direction="up" ref={ref} {...props} />;
});

export default function FullScreenDialog({ setOpen, open, pinLoc }) {
	const classes = useStyles();

	const handleClose = () => {
		setOpen(false);
	};

	return (
		<div>
			<Dialog fullScreen open={open} onClose={handleClose} TransitionComponent={Transition}>
				<AppBar className={classes.appBar}>
					<Toolbar>
						<IconButton edge="start" color="inherit" onClick={handleClose} aria-label="close">
							<CloseIcon />
						</IconButton>
						<Typography variant="h6" className={classes.title}>
							Air Pollution Report of {pinLoc}
						</Typography>
					</Toolbar>
				</AppBar>

				<Typography variant="h6" className={classes.title2}>
					Gases Contribution Sectors
				</Typography>
				<Grid container direction="row" justify="center" alignItems="flex-start" spacing={7}>
					<Grid item xs={6} sm={3}>
						<Paper className={classes.paper} elevation={20}>
							<CODough pinLoc={pinLoc} data={coRep} />
						</Paper>
					</Grid>
					<Grid item xs={6} sm={3}>
						<Paper className={classes.paper} elevation={20}>
							<NO2Dough pinLoc={pinLoc} data={no2Rep} />
						</Paper>
					</Grid>
					<Grid item xs={6} sm={3}>
						<Paper className={classes.paper} elevation={20}>
							<SO2Dough pinLoc={pinLoc} data={so2Rep} />
						</Paper>
					</Grid>
				</Grid>

				<Grid container direction="row" justify="center" alignItems="flex-start" spacing={7} style={{ marginTop: "5px" }}>
					<Grid item xs={6} sm={3}>
						<Paper className={classes.paper} elevation={20}>
							<CH4Dough pinLoc={pinLoc} data={ch4Rep} />
						</Paper>
					</Grid>
					<Grid item xs={6} sm={3}>
						<Paper className={classes.paper} elevation={20}>
							<PM2Dough pinLoc={pinLoc} data={pm2Rep} />
						</Paper>
					</Grid>
					<Grid item xs={6} sm={3}>
						<Paper className={classes.paper} elevation={20}>
							<PM10Dough pinLoc={pinLoc} data={pm10Rep} />
						</Paper>
					</Grid>
				</Grid>

				<Typography variant="h6" className={classes.title2}>
					Amount Of Gases
				</Typography>
				<Grid container direction="row" justify="center" alignItems="flex-start" spacing={7}>
					<Grid item xs={12} sm={5}>
						<Paper className={classes.paper} elevation={20}>
							<Bar pinLoc={pinLoc} coRep={coRep} no2Rep={no2Rep} so2Rep={so2Rep} ch4Rep={ch4Rep} pm2Rep={pm2Rep} pm10Rep={pm10Rep} />
						</Paper>
					</Grid>
				</Grid>
			</Dialog>
		</div>
	);
}

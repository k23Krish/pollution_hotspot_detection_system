// imports
import React, { useState, useEffect } from "react";
import { Bar } from "react-chartjs-2";

export default function CODough({ pinLoc, coRep, ch4Rep, no2Rep, so2Rep, pm10Rep, pm2Rep }) {
	const [data, setData] = useState([]);
	useEffect(() => {
		let dump = [];
		coRep.forEach((loc) => {
			if (pinLoc === loc.Country) {
				dump.push(loc.Total);
			}
		});
		no2Rep.forEach((loc) => {
			if (pinLoc === loc.Country) {
				dump.push(loc.Total);
			}
		});
		so2Rep.forEach((loc) => {
			if (pinLoc === loc.Country) {
				dump.push(loc.Total);
			}
		});
		ch4Rep.forEach((loc) => {
			if (pinLoc === loc.Country) {
				dump.push(loc.Total);
			}
		});
		pm2Rep.forEach((loc) => {
			if (pinLoc === loc.Country) {
				dump.push(loc.Total);
			}
		});
		pm10Rep.forEach((loc) => {
			if (pinLoc === loc.Country) {
				dump.push(loc.Total);
			}
		});
		setData([...dump]);
		// eslint-disable-next-line
	}, []);
	return (
		<Bar
			data={{
				labels: ["CO", "NO2", "SO2", "CH4", "PM2.5", "PM10"],
				datasets: [
					{
						label: "Conc",
						backgroundColor: ["#463333", "gray", "#cfb495", "silver", "#835858", "#834c69"],
						borderColor: "rgba(0,0,0,0.5)",
						borderWidth: 2,
						data: [...data]
					}
				]
			}}
			options={{
				title: {
					display: true,
					text: "Gases Concentration (tonnes)",
					fontSize: 25
				}
			}}
			width={100}
			height={70}
		/>
	);
}

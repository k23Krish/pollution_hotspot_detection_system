// imports
import React, { useState, useEffect } from "react";
import { Doughnut } from "react-chartjs-2";

export default function PM2Dough({ pinLoc, data }) {
	const [coData, setCoData] = useState([]);

	useEffect(() => {
		let dump = [];
		data.forEach((loc) => {
			if (pinLoc === loc.Country) {
				dump.push(loc.MSP);
				dump.push(loc.RTP);
				dump.push(loc.WP);
				dump.push(loc.IP);
				dump.push(loc.PSP);
				dump.push(loc.AP);
			}
		});

		setCoData({
			labels: ["Mobile Source", "Road Transport", "Waste", "Industrial", "Power Stations", "Agriculture"],
			datasets: [
				{
					label: "PM2.5 Precentage",
					fill: true,
					lineTension: 0.5,
					backgroundColor: ["#463333", "gray", "#834c69", "silver", "#835858", "#cbaf87"],
					borderColor: "rgba(0,0,0,0.5)",
					borderWidth: 1,
					data: [...dump]
				}
			]
		});
		// eslint-disable-next-line
	}, [data]);

	return (
		<Doughnut
			data={{ ...coData }}
			options={{
				title: {
					display: true,
					text: "PM2.5 Percentage Concentration",
					fontSize: 25
				},
				legend: {
					display: true,
					position: "right"
				}
			}}
			width={100}
			height={70}
		/>
	);
}

// imports
import React, { useState, useEffect } from "react";
import { Doughnut } from "react-chartjs-2";

export default function CODough({ pinLoc, data }) {
	const [coData, setCoData] = useState([]);

	useEffect(() => {
		let dump = [];
		data.forEach((loc) => {
			if (pinLoc === loc.Country) {
				dump.push(loc.MSP);
				dump.push(loc.RTP);
				dump.push(loc.PST);
				dump.push(loc.IP);
				dump.push(loc.AP);
				dump.push(loc.WP);
			}
		});

		setCoData({
			labels: ["Mobile Source", "Road Transport", "Power Stations", "Industrial", "Agriculture", "Waste"],
			datasets: [
				{
					label: "CH4 Precentage",
					fill: true,
					lineTension: 0.5,
					backgroundColor: ["#463333", "gray", "#cfb495", "silver", "#835858", "#834c69"],
					borderColor: "rgba(0,0,0,0.5)",
					borderWidth: 1,
					data: [...dump]
				}
			]
		});
		// eslint-disable-next-line
	}, [data]);

	return (
		<Doughnut
			data={{ ...coData }}
			options={{
				title: {
					display: true,
					text: "CO Percentage Concentration",
					fontSize: 25
				},
				legend: {
					display: true,
					position: "right"
				}
			}}
			width={100}
			height={70}
		/>
	);
}

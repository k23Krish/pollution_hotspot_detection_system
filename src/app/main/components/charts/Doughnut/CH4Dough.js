// imports
import React, { useState, useEffect } from "react";
import { Doughnut } from "react-chartjs-2";

export default function CH4Dough({ pinLoc, data }) {
	const [coData, setCoData] = useState([]);

	useEffect(() => {
		let dump = [];
		data.forEach((loc) => {
			if (pinLoc === loc.Country) {
				dump.push(loc.MSP);
				dump.push(loc.RTP);
				dump.push(loc.PSP);
				dump.push(loc.IP);
				dump.push(loc.AP);
				dump.push(loc.WP);
				dump.push(loc.MP);
			}
		});

		setCoData({
			labels: ["Mobile Source", "Road Transport", "Power Stations", "Industrial", "Agriculture", "Waste", "Miscellaneous"],
			datasets: [
				{
					label: "NO2 Precentage",
					fill: true,
					lineTension: 0.5,
					backgroundColor: ["#463333", "gray", "#834c69", "silver", "#835858", "#cbaf87"],
					borderColor: "rgba(0,0,0,0.5)",
					borderWidth: 1,
					data: [...dump]
				}
			]
		});
		// eslint-disable-next-line
	}, [data]);

	return (
		<Doughnut
			data={{ ...coData }}
			options={{
				title: {
					display: true,
					text: "CH4 Percentage Concentration",
					fontSize: 25
				},
				legend: {
					display: true,
					position: "right"
				}
			}}
			width={100}
			height={70}
		/>
	);
}

// imports
import React, { Fragment, useState } from "react";

// material
import { makeStyles } from "@material-ui/core/styles";
import Dialog from "@material-ui/core/Dialog";
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import IconButton from "@material-ui/core/IconButton";
import Typography from "@material-ui/core/Typography";
import CloseIcon from "@material-ui/icons/Close";
import Slide from "@material-ui/core/Slide";
import Paper from "@material-ui/core/Paper";
import Grid from "@material-ui/core/Grid";
import Button from "@material-ui/core/Button";

// Charts
import AQIB from "./hrep-charts/Bar/AQIB";
import AQIA from "./hrep-charts/Bar/AQIA";
import COLine from "./hrep-charts/Line/COLine";
import NO2Line from "./hrep-charts/Line/NO2Line";
import SO2Line from "./hrep-charts/Line/SO2Line";
import PM2Line from "./hrep-charts/Line/PM2Line";
import PM10Line from "./hrep-charts/Line/PM10Line";
import BenzeneLine from "./hrep-charts/Line/BenzeneLine";
import TouleneLine from "./hrep-charts/Line/TouleneLine";
import XyleneLine from "./hrep-charts/Line/XyleneLine";
import DeathPie from "./hrep-charts/Pie/Death";

const useStyles = makeStyles((theme) => ({
	appBar: {
		position: "relative",
		background: "#213e3b"
	},
	title: {
		marginLeft: theme.spacing(2),
		flex: 1
	},
	title2: {
		textAlign: "center",
		margin: theme.spacing(2)
	},
	paper: {
		padding: theme.spacing(2),
		textAlign: "center",
		background: "#f3f3f3",
		width: "110%",
		height: "110%",
		border: "1px solid gray"
	},
	paper1: {
		padding: theme.spacing(2),
		textAlign: "center",
		background: "#f3f3f3",
		border: "1px solid gray",
		width: "300px"
	},
	paper2: {
		padding: theme.spacing(2),
		textAlign: "center",
		background: "#f3f3f3",
		border: "1px solid gray",
		width: "500px",
		height: "250px"
	},
	rep: {
		padding: theme.spacing(2),
		margin: theme.spacing(5),
		background: "#f3f3f3",
		border: "1px solid gray",
		width: "90vw"
	}
}));

const Transition = React.forwardRef(function Transition(props, ref) {
	return <Slide direction="up" ref={ref} {...props} />;
});

export default function HealthRep({ setOpen, open, pinLoc }) {
	const classes = useStyles();
	const [open1, setOpen1] = useState(false);

	const handleClose = () => {
		setOpen(false);
	};

	return (
		<div>
			<Dialog fullScreen open={open} onClose={handleClose} TransitionComponent={Transition}>
				<AppBar className={classes.appBar}>
					<Toolbar>
						<IconButton edge="start" color="inherit" onClick={handleClose} aria-label="close">
							<CloseIcon />
						</IconButton>
						<Typography variant="h6" className={classes.title}>
							Health Report India
						</Typography>
						<Button
							variant="contained"
							onClick={(e) => {
								setOpen1(!open1);
							}}
							style={{ background: "#ffbb91", color: "#213e3b" }}>
							{open1 ? "Previous" : "Report Summary"}
						</Button>
					</Toolbar>
				</AppBar>
				{!open1 ? (
					<Fragment>
						{/* Monthly */}
						<Typography variant="h6" className={classes.title2}>
							Monthly Polluants Concentration
						</Typography>
						<Grid container direction="row" justify="center" alignItems="flex-start" spacing={10}>
							<Grid item xs={6} sm={3}>
								<Paper className={classes.paper1} elevation={20}>
									<COLine />
								</Paper>
							</Grid>
							<Grid item xs={6} sm={3}>
								<Paper className={classes.paper1} elevation={20}>
									<NO2Line />
								</Paper>
							</Grid>
							<Grid item xs={6} sm={3}>
								<Paper className={classes.paper1} elevation={20}>
									<SO2Line />
								</Paper>
							</Grid>
						</Grid>
						<Grid style={{ marginTop: "5px" }} container direction="row" justify="center" alignItems="flex-start" spacing={10}>
							<Grid item xs={6} sm={3}>
								<Paper className={classes.paper1} elevation={20}>
									<PM2Line />
								</Paper>
							</Grid>
							<Grid item xs={6} sm={3}>
								<Paper className={classes.paper1} elevation={20}>
									<PM10Line />
								</Paper>
							</Grid>
						</Grid>
						{/* Indoor */}
						<Typography variant="h6" className={classes.title2}>
							Indoor Pollutants
						</Typography>
						<Grid container direction="row" justify="center" alignItems="flex-start" spacing={10}>
							<Grid item xs={6} sm={3}>
								<Paper className={classes.paper1} elevation={20}>
									<BenzeneLine />
								</Paper>
							</Grid>
							<Grid item xs={6} sm={3}>
								<Paper className={classes.paper1} elevation={20}>
									<TouleneLine />
								</Paper>
							</Grid>
							<Grid item xs={6} sm={3}>
								<Paper className={classes.paper1} elevation={20}>
									<XyleneLine />
								</Paper>
							</Grid>
						</Grid>

						{/* Lockdown */}
						<Typography variant="h6" className={classes.title2}>
							Lockdown Effects
						</Typography>
						<Grid container direction="row" justify="center" alignItems="flex-start" spacing={10}>
							<Grid item xs={6} sm={3}>
								<Paper className={classes.paper} elevation={20}>
									<AQIB />
								</Paper>
							</Grid>
							<Grid item xs={6} sm={3}>
								<Paper className={classes.paper} elevation={20}>
									<AQIA />
								</Paper>
							</Grid>
						</Grid>
					</Fragment>
				) : (
					<Grid container direction="row" justify="left" alignItems="flex-start" spacing={10}>
						<Grid item xs={12} sm={6}>
							<Paper className={classes.rep} elevation={20}>
								<Typography variant="h6" component="h6">
									Seasonal Trends in India
								</Typography>
								<Typography variant="subtitle1">
									1. NO2: There is a clear trend that the pollution level of NO2 in India falls in Summer and starts rising and reaches its highest
									peak in Winter. During the monsoon season, it lies in the middle of its peak. NO2 level started increasing from 2017 and its median
									value in 2020 is comparatively lesser as compared to the previous years, thereby giving a clear indication that there might be a
									reduction of pollution level because of complete lockdown.
								</Typography>
								<Typography variant="subtitle1">
									2. SO2: There is a clear trend that pollution levels in India fall in Summer and start rising and reach their highest peak in
									winter. During monsoon, it's in the middle of its peak. SO2 levels started increasing from 2016 and its median value in 2020 is
									comparatively greater as compared to other gases in 2020, giving a clear indication that there might be an impact of SO2 on
									pollution level during complete lockdown.
								</Typography>
								<Typography variant="subtitle1">
									3. BTX (Indoor Pollutants): Levels decline at the end of the rainy season. Levels in summer are greater than in monsoon, which again
									reduces at the end of the season. Level is at its highest peak in the winter season. There is a sudden peak and reduction level
									throughout the year from 2015-2020.
								</Typography>
								<Typography variant="h6" component="h6">
									Effects of Lockdown on Pollution Levels
								</Typography>
								<Typography variant="subtitle2">
									1. The black vertical line shows the date on which the first phase of lockdown came into effect in India.
								</Typography>
								<Typography variant="subtitle2">
									2. The graph shows the variation of various pollutant levels, from Jan 2019 onwards till date.
								</Typography>
								<Typography variant="subtitle2">
									3. Apparently, all the above Indian cities seem to have dangerously high levels of pollution.
								</Typography>
								<Typography variant="subtitle2">
									4. Clearly, there appears to be a rapid decline in pollution after 25th March, 2020 in all the cities under consideration.
								</Typography>
								<Typography variant="h6" component="h6">
									Negative Effects Of Pollution
								</Typography>
								<DeathPie />
							</Paper>
						</Grid>
					</Grid>
				)}
			</Dialog>
		</div>
	);
}

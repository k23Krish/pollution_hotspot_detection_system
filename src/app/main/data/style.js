import { coStyle } from "./map-styles/costyle";
import { no2Style } from "./map-styles/no2style";
import { so2Style } from "./map-styles/so2style";
import { ch4Style } from "./map-styles/ch4style";
import { o3Style } from "./map-styles/o3style";

export default [coStyle, no2Style, so2Style, ch4Style, o3Style];

import data18 from "./heatmap-data/heat18.geojson";
import data19 from "./heatmap-data/heat19.geojson";
import data20 from "./heatmap-data/heat20.geojson";

export default [data18, data19, data20];

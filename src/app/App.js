// imports
import React from "react";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
// import { useCookies } from "react-cookie";

// components
import Homepage from "./main";

function App() {
	return (
		<Router>
			<Switch>
				<Route exact path="/">
					<Homepage />
				</Route>
			</Switch>
		</Router>
	);
}

export default App;
